$(document).ready(function () {
  /* $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); */

  $('.phone').inputmask('+7(999)999-99-99');

  $('.s-header__toggle').on('click', function (e) {
    $('.s-header__info').slideToggle('fast', function (e) {});
    $('.s-nav').slideToggle('fast', function (e) {});
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.s-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.s-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.s-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.s-reviews__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
        dots: false
      }
    }]
  });

  $('#slider-days').rangeslider({
    polyfill: false,

    onInit: function () {},

    onSlide: function (position, value) {
      $('#input-days').val(value);
    },
  });

  $('#input-days').on('input', function () {
    $('#slider-days').val($(this).val()).change();
  });

  $('#modal-slider-days').rangeslider({
    polyfill: false,

    onInit: function () {},

    onSlide: function (position, value) {
      $('#modal-input-days').val(value);
    },
  });

  $('#modal-input-days').on('input', function () {
    $('#modal-slider-days').val($(this).val()).change();
  });

  $('.quiz-go-one').on('click', function (e) {
    e.preventDefault();
    $('#quiz-step-one').show();
    $('#quiz-step-two').hide();
    $('#quiz-step-three').hide();
  });

  $('.quiz-go-two').on('click', function (e) {
    e.preventDefault();
    $('#quiz-step-one').hide();
    $('#quiz-step-two').show();
    $('#quiz-step-three').hide();
  });

  $('.quiz-go-three').on('click', function (e) {
    e.preventDefault();
    $('#quiz-step-one').hide();
    $('#quiz-step-two').hide();
    $('#quiz-step-three').show();
  });

  var isOpened = true;

  $('.s-brands__button').on('click', function (e) {
    e.preventDefault();

    if (isOpened) {
      $(this).html('Скрыть туроператоров');
      $('.s-brands__cards').toggleClass('s-brands__cards_opened');
      isOpened = false;
    } else {
      $(this).html('Все туроператоры');
      $('.s-brands__cards').toggleClass('s-brands__cards_opened');
    }
  });

  $('.s-papers__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
        dots: false
      }
    }]
  });

  $('.a-logo__nav').on('click', function (e) {
    $('.a-top').slideToggle('fast', function (e) {});
    $('.a-info').slideToggle('fast', function (e) {});
    $('.a-nav').slideToggle('fast', function (e) {});
  });

  /*  $('.d-service-aside__dropsubul').on('click', function(e) {
    if (
      !$(e.target).closest('.d-service-aside__subul').length &&
      !$(e.target).is('.d-service-aside__subul')
    ) {
      $(this)
        .next()
        .slideToggle('fast', function(e) {
          $(this)
            .prev()
            .parent()
            .toggleClass('d-service-aside__dropsubul_opened');
        });
    }
  });

  $('.d-top__menu').on('click', function(e) {
    $('.d-nav').slideToggle('fast', function(e) {});
  }); */
});
